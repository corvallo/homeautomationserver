import db from 'diskdb';
import path from 'path';


class BasicModel {

    constructor() {        
        this._class_name = this.constructor.name.toLocaleLowerCase();
        this.db = this._db_connect();
    }
    _db_connect() {        
        return db.connect(path.join(__dirname, '../database', ), [this._class_name]);       
    }
}

export default BasicModel;