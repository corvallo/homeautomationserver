import express from 'express';
import path from 'path';
import User from '../models/users';

let home = express.Router();

home.get(['/', '/home'], (req, res, next) => {
    let user = new User();
    user.index();
    res.sendFile(path.join(__dirname, '../public', 'index.html'));    
});

home.get(['/dashboard'],(req,res,next) => {
    res.send("DASHBOARD2");
});

export default home;