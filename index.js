import express from 'express';
import http from 'http';
import url from 'url';
import WebSocket from 'ws';
import { home } from './routes';

let app = express();
app.use('/', home);
  
const server = http.createServer(app);
const ws_server = new WebSocket.Server({ server });

ws_server.on('connection', function connection(socket, req) {
    const location = url.parse(req.url, true);
    socket.on('message', function incoming(message) {
        console.log('received: %s', message);
    });
    socket.send('something');
});

server.listen(3000, function listening() {
    console.log('Listening on %d', server.address().port);
});
